# msc
monarrk's [snowflake](https://github.com/snowflake-language/snowflake) compiler/interpreter

## building
install [cabal](https://www.haskell.org/cabal/), then run `make`. to install to `~/.cabal/bin`, run `make install`.
