module Main where

import System.IO
import System.Environment

import Parser
import Generator

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> hPutStrLn stderr "Error: please provide a file to execute"
        _ -> do
            content <- readFile $ args!!0
            let t = parse content
            -- putStrLn $ show t
            string <- generate t
            writeFile ((args!!0) ++ ".ssa") (show string)
