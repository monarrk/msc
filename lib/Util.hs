{-# LANGUAGE RankNTypes, PolyKinds, DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Util
    (
        sfError
    ) where

import GHC.Types (Char, RuntimeRep, TYPE)
import GHC.Stack.Types

sfError :: forall (r :: RuntimeRep). forall (a :: TYPE r). HasCallStack => String -> String -> Int -> Int -> a
sfError tag msg l c = do 
    let err = "Error: " ++ tag ++ ": " ++ (show l) ++ "," ++ (show c) ++ ": " ++ msg
    error err
