module Parser where

import Data.Char
import Data.Maybe

import Util

-- | error tag
tag = "Parser.hs"

-- | an enumeration of each possible type for a token
data TokenType = Ident
                 -- sigils
                 | DoubleColon
                 | SingleArrow
                 | DoubleArrow
                 | Equal
                 | DotDot
                 | Whitespace
                 | NewLn
                 | OpenParen
                 | CloseParen
                 -- types
                 | SfInt { intValue :: Int }
                 | SfFloat { floatValue :: Float }
                 | StringLiteral
                 -- syntax patterns
                 | Match 
                 | Tag
                 | Let
                 | In
                 -- No Operation
                 | Nop
                   deriving (Show, Eq)

-- | a token itself
data Token = Token { raw :: String
                     , kind :: TokenType
                     } deriving (Show, Eq)

-- | tokenize a string
-- https://jameshfisher.com/2018/03/09/writing-a-parser-in-haskell/
tokenize :: String -> Int -> Int -> [Token]
tokenize [] l c = []

-- newline
tokenize ('\n':cs) l c = (Token "\n" NewLn) : tokenize cs (l + 1) 0

tokenize list l col
    -- ::
    | c == ':' && peek == ':' = (Token "::" DoubleColon) : tokenize peekTail l (col + 2)
    -- ->
    | c == '-' && peek == '>' = (Token "->" SingleArrow) : tokenize peekTail l (col + 2)
    -- =>
    | c == '=' && peek == '>' = (Token "=>" DoubleArrow) : tokenize peekTail l (col + 2)
    -- ** (comment)
    | c == '*' && peek == '*' = tokenize (dropWhile (/= '\n') peekTail) l col
    -- =
    | c == '=' = (Token "=" Equal) : tokenize cs l (col + 1)
    -- ..
    | c == '.' && peek == '.' = (Token ".." DotDot) : tokenize peekTail l (col + 2)
    -- (
    | c == '(' = (Token "(" OpenParen) : tokenize cs l (col + 1)
    -- )
    | c == ')' = (Token ")" CloseParen) : tokenize cs l (col + 1)
    -- string literal
    | c == '"' = (Token string StringLiteral) : tokenize dropString l (col + length string)
    -- "match"
    | word == "match" = (Token "match" Match) : tokenize dropWord l (col + length "match")
    -- "let"
    | word == "let" = (Token "let" Let) : tokenize dropWord l (col + length "let")
    -- "in"
    | word == "in" = (Token "in" In) : tokenize dropWord l (col + length "in")
    -- "tag"
    | word == "tag" = (Token "tag" Tag) : tokenize dropWord l (col + length "tag")
    -- numbers
    | isNumber c = (Token (int) (SfInt (read int))) : tokenize dropInt l (col + length int)
    -- idents
    | isSymbolChar c = (Token word Ident) : tokenize dropWord l (col + length word)
    -- whitespace
    | isSpace c = (Token [c] Whitespace) : tokenize cs l (col + 1)
    | otherwise = sfError tag ("unexpected character: " ++ [c]) l col
    where (c:cs) = list
          peek = head cs
          peekTail = tail cs
          int = c : takeWhile isNumber cs
          dropInt = dropWhile isNumber cs
          word = c : takeWhile isSymbolChar cs
          dropWord = dropWhile isSymbolChar cs
          string = takeWhile (/= '"') cs
          dropString = do
              let list = dropWhile (\a -> if a == '"' then False else if a == '\n' then (sfError tag "Unfinished string" l col) else True) cs
              if list == [] then [] else drop 1 list

isSymbolChar c = isAlphaNum c || elem c "=+<-/%_"

parse :: String -> [Token]
parse s = tokenize s 1 1
