module Generator where

import Data.HashMap.Internal hiding (map)
import Text.Read hiding (Ident)

import Parser

-- | the state of the evaluator
data EvalState = EvalState { expecting :: [TokenType]
                             , previous :: [Token]
                             , bindings :: HashMap String Type } deriving (Show)

-- | datatypes for bindings
data Type = FnSig { args :: [Type], ret :: Type }
            | Nat Int
            | TypeIdent String
             deriving (Show)

-- | turn a string into a Type
intoType :: String -> Type
intoType s = do
    case (readMaybe s) of
        Just a -> Nat a
        Nothing -> TypeIdent s

-- | a space
space = Token " " Whitespace

-- | "run" a token
run :: EvalState -> [Token] -> EvalState
run s [] = s

run state list
    | x == ident && peek == colon = run (EvalState e (x : p) (insert (raw x) (FnSig (map (\a -> intoType (raw a)) (init args)) (intoType $ raw (last args))) b)) xs
    where (x:xs) = list
          (EvalState e p b) = state
          peek = head xs
          peekTail = tail xs
          args = takeWhile (\a -> kind a == Ident) peekTail
          colon = Token "::" DoubleColon
          ident = Token (raw x) Ident

run (EvalState e p b) (tok:xs) = run (EvalState e (tok : p) b) xs

generate :: [Token] -> IO EvalState
generate list = do return $ run (EvalState [] [] empty) list
