module Universe
    (
        Universe
        , Binding
        , Bound
        , TagName
        , Tag
    ) where

import Data.HashMap.Internal
import Parser

-- | a universe which contains tags and their bindings
data Universe = Universe { bindings :: Binding
                           , tags :: HashMap TagName Tag } deriving (Show)

-- | a binding
data Binding = Binding { name :: String
                         , value :: Bound } deriving (Show)

-- | a value bound in a binding
type Bound = (Maybe [TagName], Maybe Type, Maybe [Token])

-- | a tagname
type TagName = String

-- | a representation of a tag
type Tag = [Binding]

-- | a type
data Type = FnSig { args :: [Type], ret :: Type } | TagType | Nat | Ident deriving (Show)
